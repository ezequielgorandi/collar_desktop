ventanaPrincipal.py: ventanaPrincipal.ui
	pyuic4 ventanaPrincipal.ui -o ventanaPrincipal.py

dialogoConfig.py: dialogoConfig.ui
	pyuic4 dialogoConfig.ui -o dialogoConfig.py

dialogoParam.py: configParametros.ui
	pyuic4 configParametros.ui -o dialogoParam.py

acercaDe.py: acercade.ui
	pyuic4 acercade.ui -o acercaDe.py

recursos_rc.py: recursos.qrc
	pyrcc4 recursos.qrc > recursos_rc.py

.PHONY: all
all: 
	ventanaPrincipal.py dialogoConfig.py dialogoParam.py acercaDe.py recursos_rc.py
