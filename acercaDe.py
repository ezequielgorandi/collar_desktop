# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'acercade.ui'
#
# Created: Tue Sep 30 13:51:43 2014
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_AcercaDe(object):
    def setupUi(self, AcercaDe):
        AcercaDe.setObjectName(_fromUtf8("AcercaDe"))
        AcercaDe.setWindowModality(QtCore.Qt.ApplicationModal)
        AcercaDe.resize(565, 205)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(AcercaDe.sizePolicy().hasHeightForWidth())
        AcercaDe.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/images/inta.jpg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        AcercaDe.setWindowIcon(icon)
        self.label = QtGui.QLabel(AcercaDe)
        self.label.setGeometry(QtCore.QRect(0, 10, 541, 91))
        self.label.setText(_fromUtf8(""))
        self.label.setPixmap(QtGui.QPixmap(_fromUtf8(":/images/logolab.png")))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(AcercaDe)
        self.label_2.setGeometry(QtCore.QRect(60, 110, 431, 61))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial"))
        font.setPointSize(18)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(AcercaDe)
        self.label_3.setGeometry(QtCore.QRect(230, 170, 51, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_ver = QtGui.QLabel(AcercaDe)
        self.label_ver.setGeometry(QtCore.QRect(290, 170, 41, 20))
        self.label_ver.setObjectName(_fromUtf8("label_ver"))

        self.retranslateUi(AcercaDe)
        QtCore.QObject.connect(AcercaDe, QtCore.SIGNAL(_fromUtf8("destroyed()")), AcercaDe.close)
        QtCore.QMetaObject.connectSlotsByName(AcercaDe)

    def retranslateUi(self, AcercaDe):
        AcercaDe.setWindowTitle(QtGui.QApplication.translate("AcercaDe", "Acerca de", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("AcercaDe", "Gestor de collares para agrotrazabilidad", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("AcercaDe", "Versión", None, QtGui.QApplication.UnicodeUTF8))
        self.label_ver.setText(QtGui.QApplication.translate("AcercaDe", "2.0", None, QtGui.QApplication.UnicodeUTF8))

import recursos_rc
