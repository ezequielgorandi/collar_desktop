#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  config.py
#  
#  Copyright 2013 julian <julian@desktopIR>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
ARCHIVO_CONFIG = 'agrotraz.conf'

import ConfigParser, os
import serial

class Configuracion():
	"""
	Clase que maneja la configuraciion de la aplicacion
	"""
	config = None
	def __init__(self):
		
		if(not self.leer()):
			self.initFile()
		
	def initFile(self):
		"""
		Inicializa el archivo de configuracion
		"""
		
		self.config = ConfigParser.ConfigParser()
		self.config.add_section('Serie')
		self.config.set('Serie', 'puerto', '0')
		self.config.set('Serie', 'long_byte', '8')
		self.config.set('Serie', 'paridad', 'No')
		self.config.set('Serie', 'bit_parada', '1')		
		self.config.set('Serie', 'tasa', '9600')
		self.config.add_section('Misc')
		self.config.set('Misc', 'dir', os.getcwd())


		archivo = open(ARCHIVO_CONFIG, 'w')	
		self.config.write(archivo)
		self.loadConfig()
		
	def leer(self):
		"""
		Intenta leer la configuración desde el archivo.
		"""
		estado = True
		
		try:
			self.config = ConfigParser.ConfigParser()
			self.config.read(ARCHIVO_CONFIG)		
	
			self.loadConfig()
	
		except:
			estado = False
		
		return estado
		
	def guardar(self):
		"""
		Intenta guardar la configuración en el archivo.
		"""
		estado = True
		
		try:			
			
			for clave in self.__dic:
				seccion,campo = clave.split(":")
				self.config.set(seccion, campo, self.__dic[clave])		
		
			archivo = open(ARCHIVO_CONFIG, 'w')	
			self.config.write(archivo)			
				
		except:
			estado = False
		
		return estado
		
	def loadConfig(self):
		"""
		Retorma todas las opciones como un diccionario
		"""
		self.__dic = {}
		secciones = ['Serie','Misc']
		for seccion in secciones:
			for campo in self.config.items(seccion):
				self.__dic[seccion+':'+campo[0]] = campo[1]		
			
	def getUsable(self):
		"""
		Traduce los parametros del valor String al valor usable
		"""	
		dict=self.__dic
		
		#Traduzco paridad
		if(dict['Serie:paridad'] == 'No'):
			dict['Serie:paridad'] = serial.PARITY_NONE
		elif (dict['Serie:paridad'] == 'Par'):
			dict['Serie:paridad'] = serial.PARITY_EVEN
		elif (dict['Serie:paridad'] == 'Impar'):
			dict['Serie:paridad'] = serial.PARITY_ODD
			
			
		#Traduzco long_byte
		if(dict['Serie:long_byte'] == '8'):
			dict['Serie:long_byte'] = serial.EIGHTBITS
		elif (dict['Serie:long_byte'] == '7'):
			dict['Serie:long_byte'] = serial.SEVENBITS
		elif (dict['Serie:long_byte'] == '6'):
			dict['Serie:long_byte'] = serial.SIXBITS
		elif (dict['Serie:long_byte'] == '5'):
			dict['Serie:long_byte'] = serial.FIVEBITS
		
		
		#Traduzco bit_parada
		if(dict['Serie:bit_parada'] == '1'):
			dict['Serie:bit_parada'] = serial.STOPBITS_ONE
		elif (dict['Serie:bit_parada'] == '1.5'):
			dict['Serie:bit_parada'] = serial.STOPBITS_ONE_POINT_FIVE
		elif (dict['Serie:bit_parada'] == '2'):
			dict['Serie:bit_parada'] = serial.STOPBITS_TWO
		
		#Traduzco tasa
		dict['Serie:tasa'] = int(dict['Serie:tasa'])
		
		#Si el valor del puerto es '0' se define como 0 para que la biblioteca abra el primer puerto que encuentre
		if(dict['Serie:puerto'] == '0' or dict['Serie:puerto'] == ''):
			dict['Serie:puerto'] = 0
		
		
		return dict
		
		
	#Defino estos metodos para que se  pueda tratar al objeto como diccionario
	def __iter__(self):
		return self.__dic.__iter__()
		
	def __getitem__(self,clave):
		return self.__dic[clave]
	
	def __setitem__(self,clave,valor):
		self.__dic[clave] = valor
		

    
		

def main():
	a = configuracion()
	return 0

if __name__ == '__main__':
	main()

