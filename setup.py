#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  setup.py
#  
#  Copyright 2013 julian <julian@desktopIR>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
from distutils.core import setup
import py2exe

setup(name="Agrotrazabilidad",
      version="0.1",
      author="Julián Aicardo",
      author_email="jaicardo@cnia.inta.gov.ar",
      url="http://agroelectronica.inta.gov.ar/agrotrazabilidad",
      license="GNU General Public License (GPL)",
      packages=['agrotraz'],
      package_data={"agrotraz": ["ui/*"]},
      scripts=["bin/agrotraz"],
      windows=[{"script": "bin/agrotraz"}],
      options={"py2exe": {"skip_archive": True, "includes": ["sip"]}})

