# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'configParametros.ui'
#
# Created: Fri Oct  3 13:12:29 2014
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_DialogParam(object):
    def setupUi(self, DialogParam):
        DialogParam.setObjectName(_fromUtf8("DialogParam"))
        DialogParam.setWindowModality(QtCore.Qt.ApplicationModal)
        DialogParam.resize(376, 328)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(DialogParam.sizePolicy().hasHeightForWidth())
        DialogParam.setSizePolicy(sizePolicy)
        self.gridLayoutWidget_2 = QtGui.QWidget(DialogParam)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(10, 20, 361, 201))
        self.gridLayoutWidget_2.setObjectName(_fromUtf8("gridLayoutWidget_2"))
        self.gridLayout_2 = QtGui.QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setMargin(0)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.spinBoxID_Collar = QtGui.QSpinBox(self.gridLayoutWidget_2)
        self.spinBoxID_Collar.setEnabled(True)
        self.spinBoxID_Collar.setMinimum(1)
        self.spinBoxID_Collar.setMaximum(99999)
        self.spinBoxID_Collar.setObjectName(_fromUtf8("spinBoxID_Collar"))
        self.gridLayout_2.addWidget(self.spinBoxID_Collar, 1, 1, 1, 1)
        self.label_17 = QtGui.QLabel(self.gridLayoutWidget_2)
        self.label_17.setObjectName(_fromUtf8("label_17"))
        self.gridLayout_2.addWidget(self.label_17, 1, 0, 1, 1)
        self.spinBoxSerie = QtGui.QSpinBox(self.gridLayoutWidget_2)
        self.spinBoxSerie.setEnabled(True)
        self.spinBoxSerie.setMinimum(1)
        self.spinBoxSerie.setMaximum(99999)
        self.spinBoxSerie.setProperty("value", 1)
        self.spinBoxSerie.setObjectName(_fromUtf8("spinBoxSerie"))
        self.gridLayout_2.addWidget(self.spinBoxSerie, 0, 1, 1, 1)
        self.label_13 = QtGui.QLabel(self.gridLayoutWidget_2)
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.gridLayout_2.addWidget(self.label_13, 0, 0, 1, 1)
        self.label = QtGui.QLabel(self.gridLayoutWidget_2)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_2.addWidget(self.label, 3, 0, 1, 1)
        self.label_18 = QtGui.QLabel(self.gridLayoutWidget_2)
        self.label_18.setObjectName(_fromUtf8("label_18"))
        self.gridLayout_2.addWidget(self.label_18, 2, 0, 1, 1)
        self.spinBoxID_Estacion = QtGui.QSpinBox(self.gridLayoutWidget_2)
        self.spinBoxID_Estacion.setEnabled(True)
        self.spinBoxID_Estacion.setMinimum(1)
        self.spinBoxID_Estacion.setMaximum(99999)
        self.spinBoxID_Estacion.setObjectName(_fromUtf8("spinBoxID_Estacion"))
        self.gridLayout_2.addWidget(self.spinBoxID_Estacion, 2, 1, 1, 1)
        self.comboBoxTimeout = QtGui.QComboBox(self.gridLayoutWidget_2)
        self.comboBoxTimeout.setObjectName(_fromUtf8("comboBoxTimeout"))
        self.comboBoxTimeout.addItem(_fromUtf8(""))
        self.comboBoxTimeout.addItem(_fromUtf8(""))
        self.comboBoxTimeout.addItem(_fromUtf8(""))
        self.comboBoxTimeout.addItem(_fromUtf8(""))
        self.comboBoxTimeout.addItem(_fromUtf8(""))
        self.gridLayout_2.addWidget(self.comboBoxTimeout, 3, 1, 1, 1)
        self.horizontalLayoutWidget = QtGui.QWidget(DialogParam)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(140, 230, 231, 80))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.buttonGuardar = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.buttonGuardar.setObjectName(_fromUtf8("buttonGuardar"))
        self.horizontalLayout.addWidget(self.buttonGuardar)
        self.pushButton_2 = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.horizontalLayout.addWidget(self.pushButton_2)

        self.retranslateUi(DialogParam)
        QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL(_fromUtf8("clicked()")), DialogParam.close)
        QtCore.QMetaObject.connectSlotsByName(DialogParam)
        DialogParam.setTabOrder(self.spinBoxSerie, self.spinBoxID_Collar)
        DialogParam.setTabOrder(self.spinBoxID_Collar, self.spinBoxID_Estacion)
        DialogParam.setTabOrder(self.spinBoxID_Estacion, self.comboBoxTimeout)
        DialogParam.setTabOrder(self.comboBoxTimeout, self.buttonGuardar)
        DialogParam.setTabOrder(self.buttonGuardar, self.pushButton_2)

    def retranslateUi(self, DialogParam):
        DialogParam.setWindowTitle(QtGui.QApplication.translate("DialogParam", "Configuración de parámetros", None, QtGui.QApplication.UnicodeUTF8))
        self.label_17.setText(QtGui.QApplication.translate("DialogParam", "ID collar:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_13.setText(QtGui.QApplication.translate("DialogParam", "Nº de serie:", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("DialogParam", "Timeout GPS(seg):", None, QtGui.QApplication.UnicodeUTF8))
        self.label_18.setText(QtGui.QApplication.translate("DialogParam", "ID estacion:", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBoxTimeout.setItemText(0, QtGui.QApplication.translate("DialogParam", "60", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBoxTimeout.setItemText(1, QtGui.QApplication.translate("DialogParam", "75", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBoxTimeout.setItemText(2, QtGui.QApplication.translate("DialogParam", "90", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBoxTimeout.setItemText(3, QtGui.QApplication.translate("DialogParam", "120", None, QtGui.QApplication.UnicodeUTF8))
        self.comboBoxTimeout.setItemText(4, QtGui.QApplication.translate("DialogParam", "180", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonGuardar.setText(QtGui.QApplication.translate("DialogParam", "Guardar", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_2.setText(QtGui.QApplication.translate("DialogParam", "Cerrar", None, QtGui.QApplication.UnicodeUTF8))

