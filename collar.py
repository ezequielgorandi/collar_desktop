#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  collar.py
#  
#  Copyright 2013 julian <julian@desktopIR>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import serial
import time
import re
import sys, traceback

PROMPT = '>'
#Segundos antes de leer del dispositivo
ESPERA_SERIAL_LECT = 1
#Segundos antes de dejar de leer del dispositivo.
TIMEOUT_LECTURA = 0.5
#Segundos antes de escribir del dispositivo(necesario para obtener el prompt)
ESPERA_SERIAL_ESC = 2
#Longitud en bytes de cada registro de datos
TIMEOUT_ESCRITURA = 10
LEN_REG = 67
LEN_ESTADO = 269
LEN_BORRADO = 48

#Lista de comandos 
ESTADO_CMD = "e"
DATOS_CMD = "b"
CONFIG_CMD = "f"
CAMBIO_SERIE = "n"
CAMBIO_T_MUESTRAS = "t"

CAMBIO_ID_COLLAR = "c"
CAMBIO_ID_ESTACION = "o"
CAMBIO_TIMEOUTGPS = "m"
BORRAR_DATOS = "d"
CAMBIO_URL = "i"


CAMBIO_T_MUESTRAS_HN = "h"
CAMBIO_INICIO_HN = "1"
CAMBIO_FIN_HN = "2"
CAMBIO_HN = "K"


FIRM_VER_HN = 3.0


class Collar():
	"""
	Clase para interactuar con el collar a traves del puerto serie
	"""
	__serialDev = None
	__serialPort = 0
	__ultimoLeido = ''
	__ultimoError = None
	__progreso = False
	conectado = False
	
	def __init__(self,autoConectar = False, puerto=0, paridad=serial.PARITY_NONE, velocidad=9600, bitParada=serial.STOPBITS_ONE, lenByte=8):
		
		#Configuarcion del puerto serie
		
		self.paridad = paridad
		self.velocidad = velocidad
		self.bitParada = bitParada
		self.lenByte = lenByte
		
		if(puerto == None):
			self.autoDetectarPuerto()
		else:
			self.puerto = puerto
			
		if(autoConectar):
			self.conectar()
			
	def setConfig(self,puerto=None, paridad=None, velocidad=None, bitParada=None, lenByte=None):
		"""
		Establece los parametros para la conexion al puerto serie-
		"""		
		#Configuarcion del puerto serie
		self.puerto=puerto
		
		if(paridad != None):
			self.paridad = paridad
		if(velocidad != None):
			self.velocidad = velocidad
		if(bitParada != None):
			self.bitParada = bitParada
		if(lenByte != None):
			self.lenByte = lenByte

	def setFuncProgreso(self,funcProgreso,bytesTotal):	
		self.__funcProgreso = funcProgreso
		if(funcProgreso != None):
			self.__progreso = True
			self.__bytesTotal = float(bytesTotal)
			self.__funcProgreso(0)
		else:
			self.__progreso = False
			self.__bytesTotal = 0
			
	def avanzaProgreso(self,bytes):
		
		if(self.__progreso):			
			self.__funcProgreso(int((bytes/self.__bytesTotal)*100))
			
	def progresoCompleto(self):
		if(self.__progreso):
			self.__funcProgreso(100)
			
	def conectar(self):
		"""
		Conecta al puerto serie y verifica que se encuentre un collar.
		"""			
		estado = False
		try:			
			self.__serialDev = serial.Serial(port=self.puerto,
									baudrate=self.velocidad,
									parity=self.paridad,
									stopbits=self.bitParada,
									bytesize=self.lenByte
									)
			if(self.puerto == 0):
				self.puerto = self.__serialDev.portstr

			self.__progreso = False
			self.__ultimoError = "Conexión exitosa al puerto serie %s".decode("utf8") % self.__serialDev.portstr
			self.setURL()
			if(self.getConfig()):
				self.conectado = True
				estado = True			
				
		except :
			self.__ultimoError = "No se pudo abrir el puerto serie"
			traceback.print_exc()
			self.conectado = False			
			self.desconectar()
			
		return estado
			
	def desconectar(self):
		"""
		Cierra el puerto serial
		"""
		estado = True
		try:
			self.__serialDev.close()
			
			self.numSerie = 0 #'-'
			self.estacionId = 0 #'-'
			self.collarId = 0 #'-'
			self.cantReg =  0
			
			self.cantRegUsados = 0
			self.tiempoEMuestras = 0
			self.verFirmware = 0
			self.timeoutGPS = 0
			
			
			self.inicioHN = 0
			self.finHN = 0
			self.tiempoEMuestrasHN = 0
			self.horarioNocturno = None
		
			self.conectado = False
			self.__ultimoError = "Desconectado"
		except:
			estado = False
		
		return estado
	def cleanData(self,funcProgreso=None):
		exito = False
		self.setFuncProgreso(funcProgreso,LEN_BORRADO)
		try:
			if(self.escribir(BORRAR_DATOS)>0):
				info = self.leer()
				if (info.find("borrar la memoria") != -1):
					if(self.escribir_("a")>0):
						info = self.leer()
						if (info.find("Listo") != -1):
							exito = True
							self.cantReg = 0
		except:
			pass
			
		return exito
						
	def guardarConfig(self):
		"""
		Grabo los nuevos parametros al collar de agrotrazabilidad.
		"""
	
		exitos = 0
		estado = False
		totalExitos = 1		
						
		if(self.escribir(CAMBIO_T_MUESTRAS)>0):
			info = self.leer()
			if (info.find("Ingrese") != -1):
				if(self.escribir_("%03d\r" % self.tiempoEMuestras)>0):
					info = self.leer()
					if (info.find("OK") != -1):
						exitos = exitos +1

		#Si el firmware lo soporta, se setean los parametros de Horario nocturno
		if(self.verFirmware >= FIRM_VER_HN):
			totalExitos +=4
			if(self.horarioNocturno != self.__horarioNocturno):
				if(self.escribir(CAMBIO_HN)>0):		
					info = self.leer()
					#~ print info
					if (info.find("Uso Horario Nocturno") != -1):
						exitos = exitos +1
			else:
				exitos = exitos +1
					
			if(self.escribir(CAMBIO_T_MUESTRAS_HN)>0):				
				info = self.leer()
				#~ print info
				if (info.find("Ingrese") != -1):
					if(self.escribir_("%03d\r" % self.tiempoEMuestrasHN)>0):
						info = self.leer()
						#~ print info
						if (info.find("OK") != -1):
							exitos = exitos +1
							
			if(self.escribir(CAMBIO_INICIO_HN)>0):				
				info = self.leer()
				#~ print info
				if (info.find("Ingrese") != -1):
					if(self.escribir_("%02d\r" % self.inicioHN)>0):
						info = self.leer()
						#~ print info
						if (info.find("OK") != -1):
							exitos = exitos +1

			
			if(self.escribir(CAMBIO_FIN_HN)>0):				
				info = self.leer()
				#~ print info
				if (info.find("Ingrese") != -1):
					if(self.escribir_("%02d\r" % self.finHN)>0):
						info = self.leer()
						#~ print info
						if (info.find("OK") != -1):
							exitos = exitos +1			
			
			
			
			
		#Si alguno no se guardo debo avisar al usuario
		if(exitos == totalExitos):
			estado = True
		
		return estado
	def escribirParametros(self):
		exitos = 0
		estado = False
		
		if(self.escribir(CAMBIO_SERIE)>0):
			info = self.leer()
			if (info.find("Ingrese") != -1):
				if(self.escribir_("%05d\r" % self.numSerie)>0):
					info = self.leer()
					if (info.find("OK") != -1):
						exitos = exitos +1
		
		if(self.escribir(CAMBIO_ID_COLLAR)>0):				
			info = self.leer()
			if (info.find("Ingrese") != -1):
				if(self.escribir_("%05d\r" % self.collarId)>0):
					info = self.leer()
					if (info.find("OK") != -1):
						exitos = exitos +1					
						
		
		if(self.escribir(CAMBIO_ID_ESTACION)>0):				
			info = self.leer()
			if (info.find("Ingrese") != -1):
				if(self.escribir_("%05d\r" % self.estacionId)>0):
					info = self.leer()
					if (info.find("OK") != -1):
						exitos = exitos +1
						
		if(self.escribir(CAMBIO_TIMEOUTGPS)>0):				
			info = self.leer()
			if (info.find("Ingrese") != -1):
				if(self.escribir_("%d" % self.timeoutGPS)>0):
					info = self.leer()
					if (info.find("OK") != -1):
						exitos = exitos +1						
				
				
		#Si alguno no se guardo debo avisar al usuario
		if(exitos == 4):
			estado = True
		
		return estado
	def leer(self):
		"""
		Lee datos del dispositivo serial
		"""
		datos = ''
		cont = 0
		if(self.__serialDev != None and self.__serialDev.isOpen()):
			
			time.sleep(ESPERA_SERIAL_LECT)
			
			porLeer = self.__serialDev.inWaiting()
			self.__timeout(TIMEOUT_LECTURA,init=True)
			while(porLeer != 0 or not self.__timeout(TIMEOUT_LECTURA)):
				#Si hay datos en el buffer los leo
				if(porLeer !=0):
					try:
						datos += self.__serialDev.read(porLeer)
					except:
						traceback.print_exc()

					self.__ultimoLeido = datos[-1]
				
					self.avanzaProgreso(len(datos))
					self.__timeout(TIMEOUT_LECTURA,init=True)
									
				#actualizo timeout
				porLeer = self.__serialDev.inWaiting()				
			
			self.__ultimoError = "Se leyeron %d bytes del dispositivo." % len(datos)
			#self.progresoCompleto()
		else:			
			self.__ultimoError = "El puerto serie no está abierto.".decode("utf8")
			
		return datos
		
	def __timeout(self,max,init=False):
		"""
		Se utiliza para establecer timeouts
		"""
		estado = False
		if(init):
			self.__timeoutVal = time.time()
		elif(time.time()-self.__timeoutVal >= max):
			estado = True
			#print "Timeout!"
			
		return estado		
			
	def escribir_(self, datos):
		"""
		Envia la cadena datos al dispositivo serial sin esperar por el prompt
		"""
		count = 0
		try:
			count = self.__serialDev.write(datos)
			self.__ultimoError = "Se escribieron %d bytes en el dispositivo." % count
		except:
			traceback.print_exc()

		return count
		
		
	def escribir1(self, datos):
		"""
		Envia la cadena datos al dispositivo serial esperando antes por el prompt(DEPRECATED)
		"""
		estado = None
		
		if(self.__serialDev != None and self.__serialDev.isOpen()):
		
			lenBuffer = self.__serialDev.inWaiting()
			#Bucle necesario por a los 10 segundos de inactividad quita el prompt
			#Una vez que tengo el prompt puedo escribir al dispositivo			
			while(lenBuffer > 0 or self.__ultimoLeido != PROMPT):	
				if(self.__ultimoLeido != PROMPT):					
					self.__serialDev.write('\r')
					time.sleep(ESPERA_SERIAL_ESC)
					lenBuffer = self.__serialDev.inWaiting()
					
				temp = self.__serialDev.read(lenBuffer)
				if(len(temp)>0):				
					self.__ultimoLeido = temp[-1]
					lenBuffer = 0			
					
			estado = self.escribir_(datos)
			
		else:
			self.__ultimoError = "El puerto serie no está abierto.".decode("utf8")
			
		return estado
		
		
	def escribir(self, datos):
		"""
		Envia la cadena datos al dispositivo serial esperando antes por el prompt
		"""
		estado = 0
		
		if(self.__serialDev != None and self.__serialDev.isOpen()):
		
			lenBuffer = self.__serialDev.inWaiting()
			self.__serialDev.flush()
			#Pido el prompt
			try:
				self.__serialDev.write('\r')
			except:
				traceback.print_exc()

			time.sleep(ESPERA_SERIAL_ESC)
			#print self.__serialDev.write('\n')
			
			
			self.__timeout(TIMEOUT_ESCRITURA,init=True)
			buscoPrompt = True;
			
			#Bucle necesario por a los 10 segundos de inactividad quita el prompt
			#Una vez que tengo el prompt puedo escribir al dispositivo			
			while(not self.__timeout(TIMEOUT_ESCRITURA) and buscoPrompt):
				
				lenBuffer = self.__serialDev.inWaiting()
				
				if(lenBuffer > 0 ):
					try:
						temp = self.__serialDev.read(lenBuffer)										
						self.__ultimoLeido = temp[-1]
						
						if(self.__ultimoLeido != PROMPT):
							self.__serialDev.write('\r')
							self.__timeout(TIMEOUT_ESCRITURA,init=True)			
						else:
							buscoPrompt = False				
					except:
						traceback.print_exc()			
			
			if(not buscoPrompt):
				estado = self.escribir_(datos)
			else:
				print "No se encuentra el prompt!"
			
			
		else:
			self.__ultimoError = "El puerto serie no está abierto.".decode("utf8")
			
		return estado
			
			
	
	def autoDetectarPuerto(self):
		"""
		Detecta automaticamente en que puerto serie se encuentra conectado el collar
		"""
		pass
		
	def getError(self):
		"""
		Retorna el estado de la última operación realizada
		"""
		return self.__ultimoError
		
	def getEstadoStr(self,funcProgreso=None):
		"""
		Retorna el estado del collar como cadena de texto
		"""
		estadoStr = None
		self.setFuncProgreso(funcProgreso,LEN_ESTADO)
		if(self.escribir(ESTADO_CMD)>0):				
				estadoStr = self.leer()
					
		return estadoStr
		
	def getDatosStr(self,funcProgreso=None):
		"""
		Retorna los registros de datos del collar como cadena de texto
		"""
		self.setFuncProgreso(None,0)
		datoStr = None
		#Obtengo la cantidad de registros por si se tomó alguno más desde la última vez que se pidió
		if(self.getConfig()):
			
			bytesEsperados = LEN_REG*self.cantReg+113
			self.setFuncProgreso(funcProgreso,bytesEsperados)

			if(self.escribir(DATOS_CMD)>0):				
				datoStr = self.leer()
						
				if(len(datoStr) != bytesEsperados):
					return None
				
				datoStr = datoStr[datoStr.find("ID Est."):datoStr.find("Fin")-len(datoStr)-2]
			
		return datoStr
		
	def getConfig(self):
		estado = True
		try:
			
			self.horarioNocturno = None
			info = ''
			if(self.escribir(CONFIG_CMD)>0):				
				info = self.leer()		
				config = self.campos2Dict(info)				
					
		
			self.numSerie = int(config['Num Serie'])
			self.estacionId = int(config['ID Estacion'])
			self.collarId = int(config['ID Collar'])
			self.tiempoEMuestras = int(config['Tiempo entre Muestras'])
			self.verFirmware = float(config['Version Firmware'])
			self.timeoutGPS =  int(config['Time Out del GPS'][0:3])
			
			if(self.verFirmware >= FIRM_VER_HN):
				self.inicioHN = int(config['Inicio Horario Nocturno'][:2])
				self.finHN = int(config['Fin Horario Nocturno'][:2])	
				self.tiempoEMuestrasHN = int(config['Tiempo entre Muestras Nocturno'])
			else:
				self.inicioHN = 0
				self.finHN = 0
				self.tiempoEMuestrasHN = 0
				self.horarioNocturno = None
			
			
			info = ''
			if(self.escribir(ESTADO_CMD)>0):				
				info = self.leer()
				estado = self.campos2Dict(info)
				
				self.cantReg = int(estado["Cantidad de memoria usada"][0:4])
				self.cantRegUsados = int(estado["Cantidad de memoria usada"][8:12])
				
				if(self.verFirmware >= FIRM_VER_HN):				
					self.horarioNocturno = True if estado['Estado de Modo Horario Nocturno'] == "ACTIVADO" else False					
					self.__horarioNocturno = self.horarioNocturno
			else:
				estado = False
			
		except:
			estado=False
			traceback.print_exc()
			self.__ultimoError = "No se pudo obtener información del collar".decode("utf8")
			
		return estado
		
	def campos2Dict(self,buffer):
		config = {}
		for propiedad in buffer.split('\n\r'):
			if(propiedad != ''):
				claveValor = re.split(':|=',propiedad)
				if(len(claveValor)>1):					
					config[claveValor[0].strip()] = claveValor[1].strip()		
		
		return config
	def setURL(self):
		"""
		Fija una URL por defecto para que el parseo de los datos en la configuración no falle
		"""
		exitos = 0
		estado = False
		
		if(self.escribir(CAMBIO_URL)>0):
			info = self.leer()
			if (info.find("Ingrese") != -1):
				if(self.escribir_("127.0.0.1\r")>0):
					info = self.leer()
					if (info.find("OK") != -1):
						estado = True
		
		return estado
		

		



def main():
	
	return 0

if __name__ == '__main__':
	main()

