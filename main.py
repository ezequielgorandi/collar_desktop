#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  main.py
#  
#  Copyright 2013 julian <julian@desktopIR>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sys
from config import Configuracion
from PyQt4 import QtGui
from PyQt4 import QtCore
from ventanaPrincipal import Ui_MainWindow
from dialogoConfig import Ui_Dialog_config
from dialogoParam import Ui_DialogParam
from acercaDe import Ui_AcercaDe
from collar import Collar
import zlib
import serial.tools.list_ports as puertos

import recursos_rc

class workerThread(QtCore.QThread):
	datosListos = None
	
	def __init__(self):
		QtCore.QThread.__init__(self)
		self.datosListos = QtCore.SIGNAL("signalDatos")
		self.borradoListo = QtCore.SIGNAL("signalBorrado")
		self.progresoSignal = QtCore.SIGNAL("signalProgreso")		
		
 
	def __del__(self):
		self.wait()
	
	def progreso(self,prog):
		self.emit(self.progresoSignal,prog)	
		
	def setFunction(self,function, progreso=False,signal2Emit=None,*args, **kwargs):
		
		self.signal2Emit = signal2Emit
		self.function = function		
		self.args = args
		if(progreso):
			#Agrego como parametro la funcion que emitira las senales para que se mueva la progressBar
			kwargs['funcProgreso'] = self.progreso
		self.kwargs = kwargs
		
	def run(self):
		salida = self.function(*self.args,**self.kwargs)
		#self.emit(self.datosListos,salida)
		self.emit(self.signal2Emit,salida)
		return

class DialogoParametros(QtGui.QDialog):
	
	def __init__(self,collar):
		QtGui.QDialog.__init__(self)
		self.ui = Ui_DialogParam()
		self.ui.setupUi(self)
		self.setFixedSize(self.size())    
		
		self.ui.buttonGuardar.clicked.connect(self.guardar)
	
		self.timeouts = {60:0,75:1,90:2,120:3,180:4,0:0,1:1,2:2,3:3,4:4}	
		self.collar = collar
		self.ui.spinBoxSerie.setValue(self.collar.numSerie)
		self.ui.spinBoxID_Collar.setValue(self.collar.collarId)
		self.ui.spinBoxID_Estacion.setValue(self.collar.estacionId)
	
		
		self.ui.comboBoxTimeout.setCurrentIndex(self.timeouts[self.collar.timeoutGPS])
		
		self.__hiloTrabajo = workerThread()
		self.connect(self.__hiloTrabajo, self.__hiloTrabajo.datosListos, self.finGuardar)
		self.__hiloTrabajo.setFunction(self.collar.escribirParametros,progreso=False,signal2Emit=self.__hiloTrabajo.datosListos)
				
	def guardar(self):
		self.collar.numSerie = self.ui.spinBoxSerie.value()
		self.collar.collarId = self.ui.spinBoxID_Collar.value()
		self.collar.estacionId = self.ui.spinBoxID_Estacion.value()
					
		self.collar.timeoutGPS = self.ui.comboBoxTimeout.currentIndex()
		
		self.ui.buttonGuardar.setText("Guardando...")
		self.ui.buttonGuardar.setEnabled(False)
		
		self.__hiloTrabajo.start()
		
		
		
		#estado = self.collar.escribirParametros()
		
			
	def finGuardar(self,estado):
			
		if(estado):			
			QtGui.QMessageBox.information(self, 'Modificar parámetros'.decode("utf8"),
	   "Se actualizaron con éxito los parametros del collar.".decode("utf8"))
			self.close()

		else:
			QtGui.QMessageBox.critical(self, 'Modificar parámetros'.decode("utf8"),
		"No se pudieron actualizar los parámetros del collar. Verifique la conexion al mismo e intente nuevamente.".decode("utf8"))
		self.ui.buttonGuardar.setText("Guardar")
		self.ui.buttonGuardar.setEnabled(True)
		
class ventanaConfig(QtGui.QDialog):
	"""
	Dialogo de configuracion de la aplicacion
	"""
	
	def __init__(self):
		QtGui.QDialog.__init__(self)
		self.ui = Ui_Dialog_config()
		self.ui.setupUi(self)
		self.setFixedSize(self.size())    
		self.configLista = QtCore.SIGNAL("signalConfig")

		self.configuracion = Configuracion()
		self.setModal(True)
		#Relleno el comboBox con los puertos serialies existentes
		puertosSeriales = puertos.comports()
		for port in puertosSeriales:
			if(port[2] != 'n/a'):
				self.ui.comboBox_port.addItem(port[0])
				
				
				
		#Relleno UI
		self.ui.lineEdit_WD.setText(self.configuracion['Misc:dir'])
		index = self.ui.comboBox_port.findText(self.configuracion['Serie:puerto'])
		self.ui.comboBox_port.setCurrentIndex(index)
		
		index = self.ui.comboBox_baud.findText(self.configuracion['Serie:tasa'])
		self.ui.comboBox_baud.setCurrentIndex(index)
		
		index = self.ui.comboBox_stop.findText(self.configuracion['Serie:bit_parada'])
		self.ui.comboBox_stop.setCurrentIndex(index)
		
		index = self.ui.comboBox_parity.findText(self.configuracion['Serie:paridad'])
		self.ui.comboBox_parity.setCurrentIndex(index)
		
		index = self.ui.comboBox_byteSize.findText(self.configuracion['Serie:long_byte'])
		self.ui.comboBox_byteSize.setCurrentIndex(index)
		
		
		self.ui.buttonBox.accepted.connect(self.guardar)
		self.ui.pushButton.clicked.connect(self.cambiarWD)
		
	def guardar(self):
		"""
		Guarda la configuracion actual
		"""
		
		self.configuracion['Misc:dir'] = self.ui.lineEdit_WD.text()
				
		self.configuracion['Serie:puerto'] = self.ui.comboBox_port.currentText()
		self.configuracion['Serie:tasa'] = self.ui.comboBox_baud.currentText()		
		self.configuracion['Serie:bit_parada'] = self.ui.comboBox_stop.currentText()		
		self.configuracion['Serie:paridad'] = self.ui.comboBox_parity.currentText()
		self.configuracion['Serie:long_byte'] = self.ui.comboBox_byteSize.currentText()
		
		self.configuracion.guardar()
		
		self.emit(self.configLista)
		
	def cambiarWD(self):
		
		dialogoAr = QtGui.QFileDialog(self)
		dialogoAr.setFileMode(QtGui.QFileDialog.DirectoryOnly)
		
		if(dialogoAr.exec_()):
			ruta = dialogoAr.selectedFiles()[0]			
			self.ui.lineEdit_WD.setText(ruta)
		
		
		
class ventanaAbout(QtGui.QWidget):
    def __init__(self):
		QtGui.QWidget.__init__(self)        
		self.ui = Ui_AcercaDe()
		self.ui.setupUi(self)		
	
		
class ventanaPrincipal(QtGui.QMainWindow):

    __hiloTrabajo = None
	
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        # 'Ui_MyMainWindow' is the class, that was generated by uic, 
        # naming convention is: 'Ui_' plus the name of root widget in designer
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        
        self.progressBar = QtGui.QProgressBar(self.statusBar())
        self.statusBar().addPermanentWidget(self.progressBar)
        self.progressBar.setRange(0, 100)
        
        
        self.ui.actionExportar.setEnabled(False)
        self.ui.actionBorrar.setEnabled(False)
        self.ui.actionEditar.setEnabled(False)
        self.ui.actionGuardar.setEnabled(False)
        
        self.ui.actionDesconectar.setEnabled(False)
        
        
        
        #Conectando signals con slots
        
        self.ui.actionExportar.triggered.connect(self.exportarDatosCollar)
        self.ui.actionConectar.triggered.connect(self.conectarCollar)
        self.ui.actionGuardar.triggered.connect(self.guardarCambios)
        self.ui.actionDesconectar.triggered.connect(self.desconectarCollar)
        self.ui.actionConfigurar.triggered.connect(self.mostrarVentanaConfig)
        self.ui.actionAcerca_de.triggered.connect(self.mostrarAcercade)
        self.ui.actionEditar.triggered.connect(self.habilitarEdicion)
        self.ui.actionBorrar.triggered.connect(self.borrarCollar)
        
        self.ui.spinBoxT_muestras.valueChanged.connect(self.cambioTmuestras)
        self.ui.spinBoxT_muestrasHN.valueChanged.connect(self.habilitarGuardar)
        self.ui.spinBox_inicioHN.valueChanged.connect(self.habilitarGuardar)
        self.ui.spinBox_finHN.valueChanged.connect(self.habilitarGuardar)
        self.ui.groupBox_HN.toggled.connect(self.habilitarGuardar)
					
			
        
        
        #Creo el hilo "Trabajador"
        self.__hiloTrabajo = workerThread()
        self.connect( self.__hiloTrabajo, self.__hiloTrabajo.datosListos, self.guardoDatos)
        self.connect( self.__hiloTrabajo, self.__hiloTrabajo.borradoListo, self.borrarCollarEstado)
        self.connect( self.__hiloTrabajo, self.__hiloTrabajo.progresoSignal, self.barraProgreso)
        
        #Hilo para conectar y desconectar el collar
        self.__hiloConectar = workerThread()
        self.connect( self.__hiloConectar, self.__hiloConectar.datosListos, self.mostrarInfoCollar)
        
        #Configuracion
        conf = Configuracion()
        parametros = conf.getUsable()

        #Collar
        self.__collar = Collar(autoConectar = False, puerto=parametros['Serie:puerto'], paridad=parametros['Serie:paridad'], velocidad=parametros['Serie:tasa'], bitParada=parametros['Serie:bit_parada'], lenByte=parametros['Serie:long_byte'])
        
        self.__hiloConectar.setFunction(self.__collar.conectar,progreso=False,signal2Emit=self.__hiloTrabajo.datosListos)
        
        
        #hotKeys = QtGui.QShortcut(QtGui.QKeySequence("Ctrl+C"), self, None, self.configParametros)
        self.hotKeys = QtGui.QShortcut(self)
        self.hotKeys.setKey("Ctrl+Shift+Home")
        self.connect(self.hotKeys, QtCore.SIGNAL("activated()"), self.configParametros)
        
    def configParametros(self):		
		if(self.__collar.conectado):
			self.__dialogoParametros = DialogoParametros(self.__collar)
			#self.connect( self.__dialogoParametros, self.__dialogoParametros.configLista, self.recargarConfig)

			self.__dialogoParametros.show()
		
    def mostrarAcercade(self):
		self.__dialogoAcercade = ventanaAbout()		
		self.__dialogoAcercade.setFixedSize(self.__dialogoAcercade.size())
		
		self.__dialogoAcercade.show()		
    		
    def cambioTmuestras(self,valor):
		self.__collar.tiempoEMuestras = valor
		self.ui.actionGuardar.setEnabled(True)	
	
    def habilitarGuardar(self,valor):
		self.ui.actionGuardar.setEnabled(True)	
		
    def guardarCambios(self):
		
		
		self.ui.statusbar.showMessage("Guardando cambios...".decode("utf8"))
		
		if(self.__collar.horarioNocturno != None):			
			self.__collar.horarioNocturno = self.ui.groupBox_HN.isChecked()
			
			self.__collar.tiempoEMuestrasHN = self.ui.spinBoxT_muestrasHN.value()
			self.__collar.inicioHN = self.ui.spinBox_inicioHN.value()
			self.__collar.finHN = self.ui.spinBox_finHN.value()
				
		
		estado = self.__collar.guardarConfig()
		if(estado):
			self.ui.actionGuardar.setEnabled(False)
			self.habilitarEdicion()
			self.ui.statusbar.showMessage("Cambios guardados con éxito".decode("utf8"))
			if(self.ui.actionEditar.text().contains("Deshabilitar")):
				self.ui.actionEditar.trigger()
			self.barraProgreso(100)
		else:
			self.ui.statusbar.showMessage("Error al guardar los cambios".decode("utf8"))
		
    def habilitarEdicion(self):
		
		if(self.ui.actionEditar.text().contains("Habilitar")):			
			self.ui.spinBoxT_muestras.setEnabled(True)
			if(self.__collar.horarioNocturno != None):				
				self.ui.groupBox_HN.setEnabled(True)
				self.ui.spinBoxT_muestrasHN.setEnabled(True)
				self.ui.spinBox_inicioHN.setEnabled(True)
				self.ui.spinBox_finHN.setEnabled(True)
				
			self.ui.actionEditar.setText("Deshabilitar edicion")
			
		else:			
			self.ui.spinBoxT_muestras.setEnabled(False)	
			if(self.__collar.horarioNocturno != None):				
				self.ui.groupBox_HN.setEnabled(False)
				self.ui.spinBoxT_muestrasHN.setEnabled(False)	
				self.ui.spinBox_inicioHN.setEnabled(False)
				self.ui.spinBox_finHN.setEnabled(False)
			self.ui.actionEditar.setText("Habilitar edicion")
		
    def mostrarVentanaConfig(self):
		"""
		Muestra la ventana de configuracion de la aplicacion
		"""
		self.__dialogoConfig = ventanaConfig()		
		self.connect( self.__dialogoConfig, self.__dialogoConfig.configLista, self.recargarConfig)

		self.__dialogoConfig.show()
		
    def recargarConfig(self):
		"""
		Recarga la configuiración una vez editada.
		"""
		conf = Configuracion()
		parametros = conf.getUsable()
        
		self.__collar.setConfig(puerto=parametros['Serie:puerto'], paridad=parametros['Serie:paridad'], velocidad=parametros['Serie:tasa'], bitParada=parametros['Serie:bit_parada'], lenByte=parametros['Serie:long_byte'])
        
		
    def borrarCollar(self):
		respuesta = QtGui.QMessageBox.question(self, 'Borrado de datos',
            "Está a punto de borrar todos los datos del collar. Esta acción es irreversible. ¿Desea Continuar?".decode("utf8"), QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)
		

		if (respuesta == QtGui.QMessageBox.Yes):
			
			
			self.__hiloTrabajo.setFunction(self.__collar.cleanData,progreso=True,signal2Emit=self.__hiloTrabajo.borradoListo)
			self.barraProgreso(0)
			self.ui.statusbar.showMessage("Borrando datos...")
			self.__hiloTrabajo.start()
			
			
		
            
    def borrarCollarEstado(self,estado):
		if(estado):
			self.ui.registros_label.setText(str(self.__collar.cantReg)+"/%s" % self.__collar.cantRegUsados)
			self.ui.statusbar.showMessage("Borrado exitoso.")
			QtGui.QMessageBox.information(self, 'Borrar datos',
	   "Se borraron con éxtito los registros en el collar. Recuerde recargar las baterías antes de usarlo nuevamente.".decode("utf8"))
   			

		else:
			QtGui.QMessageBox.critical(self, 'Borrar datos',
		"No se pudieron borrar los datos del collar. Verifique la conexion al mismo e intente nuevamente.".decode("utf8"))
			self.ui.statusbar.showMessage("Error al borrar datos.")
			
    def conectarCollar(self):
		if(not self.__collar.conectado):		
			self.barraProgreso(0)
			
			self.ui.statusbar.showMessage("Conectando con el dispositivo...")
			self.__hiloConectar.setFunction(self.__collar.conectar,progreso=False,signal2Emit=self.__hiloTrabajo.datosListos)
			self.__hiloConectar.start()
			self.ui.actionConectar.setEnabled(False)
			 
		
    def mostrarInfoCollar(self,estado):
		if(estado):
			self.ui.statusbar.showMessage(self.__collar.getError())

			self.ui.estacion_label.setText("%d" % self.__collar.estacionId)
			
			self.ui.labelIdcollar.setText("%d" % self.__collar.collarId)
			self.ui.spinBoxT_muestras.setValue(self.__collar.tiempoEMuestras)
			if(self.__collar.horarioNocturno != None):				
				self.ui.groupBox_HN.setChecked(self.__collar.horarioNocturno)
			else:
				self.ui.groupBox_HN.setChecked(False)
				
			self.ui.spinBoxT_muestrasHN.setValue(self.__collar.tiempoEMuestrasHN)
			self.ui.spinBox_inicioHN.setValue(self.__collar.inicioHN)
			self.ui.spinBox_finHN.setValue(self.__collar.finHN)	
			self.ui.Verfirm.setText("%.2f" % self.__collar.verFirmware)
			
			self.ui.labelSerie.setText("%d" % self.__collar.numSerie)
			
			#self.ui.collar_label.setText(self.__collar.collarId)
			self.ui.registros_label.setText(str(self.__collar.cantReg)+"/%s" % self.__collar.cantRegUsados)
			
			
			self.ui.puerto_label.setText(str(self.__collar.puerto))
			self.ui.vel_label.setText(str(self.__collar.velocidad))
			self.ui.parada_label.setText(str(self.__collar.bitParada))
			self.ui.paridad_label.setText(str(self.__collar.paridad))
			self.barraProgreso(100)			
			
		else:
			self.ui.statusbar.showMessage(self.__collar.getError())			
			QtGui.QMessageBox.critical(self, 'Conectar',
            "No se pudieron obtener los datos del collar. Verifique la conexion al mismo e intente nuevamente.".decode("utf8"))
   			self.progressBar.reset()
			
			
		if(self.__collar.conectado):
			self.ui.actionExportar.setEnabled(True)
			self.ui.actionBorrar.setEnabled(True)
			
			self.ui.actionDesconectar.setEnabled(True)
			self.ui.actionConectar.setEnabled(False)			
			self.ui.actionEditar.setEnabled(True)
			self.ui.actionGuardar.setEnabled(False)
		else:			
			self.ui.actionExportar.setEnabled(False)
			self.ui.actionBorrar.setEnabled(False)
			self.ui.actionDesconectar.setEnabled(False)
			self.ui.actionEditar.setEnabled(False)
			self.ui.actionEditar.setText("Habilitar edicion")
			self.ui.actionConectar.setEnabled(True)						
			self.ui.spinBoxT_muestras.setEnabled(False)	
			
			self.ui.groupBox_HN.setEnabled(False)
			self.ui.spinBoxT_muestrasHN.setEnabled(False)	
			self.ui.spinBox_inicioHN.setEnabled(False)
			self.ui.spinBox_finHN.setEnabled(False)	
			
			self.ui.statusbar.showMessage("Desconectado")
			

		
    def desconectarCollar(self):
		"""
		Cierra el puierto serie donde se encuentra el collar
		"""	
		if(self.__collar.conectado):
			self.__hiloConectar.setFunction(self.__collar.desconectar,progreso=False,signal2Emit=self.__hiloConectar.datosListos)
			self.__hiloConectar.start()
			self.ui.statusbar.showMessage("Desconectando...")
			
						
			
        
    def mostrarEstadoCollar(self):
				
		self.__hiloTrabajo.setFunction(self.__collar.getEstadoStr,progreso=True,signal2Emit=self.__hiloTrabajo.datosListos)
		self.barraProgreso(0)
		self.ui.statusbar.showMessage("Descargando datos del dispositivo...")
		self.__hiloTrabajo.start()
		
    def barraProgreso(self,prog):
		"""
		Actualiza la bara de progreso con el valor prog
		"""
		self.progressBar.setValue(prog)
		
		
    def exportarDatosCollar(self):
		"""
		Inicia el proceso de descarga de datos
		"""
		
		self.__hiloTrabajo.setFunction(self.__collar.getDatosStr,progreso=True,signal2Emit=self.__hiloTrabajo.datosListos)
		self.barraProgreso(0)
		self.ui.statusbar.showMessage("Descargando datos del dispositivo...")
		self.__hiloTrabajo.start()

    
    def guardoDatos(self,datos):
		self.ui.statusbar.showMessage(self.__collar.getError())
		
		if(datos != None):
		
			
			conf = Configuracion()
			#Comprimo los datos y los gurado
			comprimidos = zlib.compress(datos)
			
			#Verifico que barra se usa en la ruta(Windows o Posix)
			if(conf['Misc:dir'].find('/') != -1):
				barra = '/'
			else:
				barra = '\\'
				
			nombreArchivo = "%s%s%d-%d.bin" % (conf['Misc:dir'],barra,self.__collar.estacionId,self.__collar.collarId) 
			archivo = open(nombreArchivo,'wb')
			archivo.write(comprimidos)
			
			archivo.close()
			QtGui.QMessageBox.information(self, 'Exportar datos',
           "Se generó con éxito el archivo %s con los registros exportados. Recuerde borrar los datos en el collar y recargar las baterías antes de usarlo nuevamente.".decode("utf8") % nombreArchivo)
		else:		
			QtGui.QMessageBox.critical(self, 'Exportar datos',
           "No se han podido exportar los datos. Verifique la conexion al collar e intente nuevamente. Si el problema persiste recargue las baterías.".decode("utf8"))
			self.ui.statusbar.showMessage("Error al exportar")
			self.progressBar.reset()
    

def main():
    
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(':images/inta.jpg'))
    ventana = ventanaPrincipal()
    ventana.setFixedSize(ventana.size())
	
    #Traduce los dialogos de QT
    traductor = QtCore.QTranslator()
    traductor.load(":idiomas/qt_es.qm")
    app.installTranslator(traductor)

    ventana.show()  
    
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
